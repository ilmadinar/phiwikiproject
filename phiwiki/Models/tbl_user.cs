//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace phiwiki.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_user
    {
        public tbl_user()
        {
            this.tbl_jawaban_quiz = new HashSet<tbl_jawaban_quiz>();
            this.tbl_layanan = new HashSet<tbl_layanan>();
            this.tbl_reimburse = new HashSet<tbl_reimburse>();
            this.tbl_transaksi_buku = new HashSet<tbl_transaksi_buku>();
        }
    
        public int id_user { get; set; }
        public string nama_user { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public string nim { get; set; }
        public string password_user { get; set; }
        public string fakultas { get; set; }
        public string kampus { get; set; }
        public string no_hp { get; set; }
        public int fk_id_role { get; set; }
    
        public virtual ICollection<tbl_jawaban_quiz> tbl_jawaban_quiz { get; set; }
        public virtual ICollection<tbl_layanan> tbl_layanan { get; set; }
        public virtual ICollection<tbl_reimburse> tbl_reimburse { get; set; }
        public virtual tbl_role tbl_role { get; set; }
        public virtual ICollection<tbl_transaksi_buku> tbl_transaksi_buku { get; set; }
    }
}
