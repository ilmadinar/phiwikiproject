﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using phiwiki.Models;
using phiwiki.Models;

namespace phiwiki.Controllers
{
    public class UserController : Controller
    {

        phiwikiEntities1 db = new phiwikiEntities1();

        public ActionResult Registration()
        {
            tbl_user regis = new tbl_user();
            return View(regis);
        }

        [HttpPost]
        public ActionResult Registration(tbl_user r)
        {
            var b = db.tbl_user.Where(x => x.username.Equals(r.username)).FirstOrDefault();
            if (b != null)
            {
                TempData["error"] = "user name already exists !";
                TempData.Keep();
                return RedirectToAction("Registration");
            }
            else
            {
                if (ModelState.IsValid)
                {
                    //r.status = "No";
                    r.fk_id_role = 1;
                    db.tbl_user.Add(r);
                    db.SaveChanges();
                    SendMailRegis(r.email);


                    TempData["success"] = "check your email to confirm your account !";
                    TempData.Keep();
                    return RedirectToAction("Registration");
                }

                return View(r);
            }

        }


        public ActionResult Login()
        {
            if (Session["username"] != null)
            {
                return RedirectToAction("Index","User", new { username = Session["username"].ToString() });
            }
            else
            {
                return View();
            }

        }

        [HttpPost]
        public ActionResult Login(tbl_user j)
        {
            
            var b = db.tbl_user.Where(x => x.username.Equals(j.username) && x.password_user.Equals(j.password_user)).FirstOrDefault();
            if (b != null)
            {
                Session["id_user"] = b.id_user;
                Session["username"] = b.username;
                Session["email"] = b.email;
                Session["role"] = b.fk_id_role;
                return RedirectToAction("Index", "User", new { username = j.username });
            }
            else
            {
                TempData["failed"] = "Incorrect user name or password !";
                TempData.Keep();
                return RedirectToAction("Login");
            }

        }



        public ActionResult Forgetpass()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Forgetpass(string email)
        {
            var objUser = db.tbl_user.Where(x => x.email == email).ToList();
            tbl_forgot_password a = new tbl_forgot_password();
            if (objUser.Count==1)
            {
                foreach (var item in objUser)
                {
                    a.fk_user = item.id_user;
                    a.status = "yes";
                    a.name_user = item.nama_user;
                }
                try
                {
                    db.tbl_forgot_password.Add(a);
                    db.SaveChanges();
                    SendMailPass(email,a);
                    return View();
                }
                catch (Exception x)
                {
                    Console.WriteLine(x.Message);
                    return View();
                }
            }
            return View();
        }

        public ActionResult Logout()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Logout(tbl_user j)
        {


            return RedirectToAction("Login");

        }



        // GET: User
        public ActionResult Index(string username)
        {
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else
            {
                return View();
            }
            return View();
        }

        // GET: User/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: User/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: User/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: User/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: User/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        public ActionResult Resetpass(string id)
        {

            return View();
        }

        [HttpPost]
        public ActionResult Resetpass(tbl_user j)
        {
            TempData["reset"] = "Your password has been changed successfully.";
            TempData.Keep();
            return RedirectToAction("Login");
        }




        [HttpPost]
        public void SendMailRegis(string email)
        {
            
            if (ModelState.IsValid)
            {
                var fromAddress = new MailAddress("mdinarlatiffa32@gmail.com", "Phiwiki");
                var toAddress = new MailAddress(email);
                const string fromPassword = "hannya23";
                const string subject = "Phiwiki - Registration";
                //string Encrypt = Encryptor.EncrpytString(objForgot.id_forgot_password.ToString());
                //var body = "Click here to change your password : <a href='https://localhost:44349/Auth/NewPassword/" + objForgot.id_forgot_password + "'>Link</a>";
                var body = "Hi, "+
                    "<br/>Thanks for signing up to Phiwiki! " +
                    "<br/>" +
                    "<br/>" +
                    "<br/>To get started, click the link below to confirm your account" +
                    "<br/> <a href='https://localhost:44319/User/Login'>Link</a>" +
                    "<br/>" +
                    "<br/>--" +
                    "<br/>This email is automatically sent from Phiwiki Web";

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)

                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true
                })
                {
                    smtp.Send(message);
                }
            }
        }


        /*
        [HttpPost]
        public ActionResult SendMailAction(string email)
        {

            var objUser = db.tbl_user.Where(x => x.email == email).ToList();
            if (objUser.Count == 1)
            {
                foreach (tbl_user item in objUser)
                {
                    objForgot.fk_user = item.id_user;
                    objForgot.status = "yes";
                    objForgot.name_user = item.name_user;
                }
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (ModelState.IsValid)
                        {
                            db.table_forgot_password.Add(objForgot);
                            db.SaveChanges();
                            
                            await Task.Run(async () =>
                                    {
                                        await SendMailAsync(objForgot, email);
                                    });
                                    
                            SendMail(email);
                            transaction.Commit();
                            return RedirectToAction("Index", "User");
                        }
                        return RedirectToAction("Registration", "User");
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        Console.WriteLine(ex.Message);
                        return RedirectToAction("Registration", "User");
                    }
                }
            }
        }
        */


        [HttpPost]
        public void SendMailPass(string email, tbl_forgot_password a)
        {

            if (ModelState.IsValid)
            {
                var fromAddress = new MailAddress("mdinarlatiffa32@gmail.com", "Phiwiki");
                var toAddress = new MailAddress(email);
                const string fromPassword = "hannya23";
                const string subject = "Phiwiki - Forgot Password";
                string id = a.id_forgot_password.ToString();
                //string Encrypt = Encryptor.EncrpytString(objForgot.id_forgot_password.ToString());
                //var body = "Click here to change your password : <a href='https://localhost:44349/Auth/NewPassword/" + objForgot.id_forgot_password + "'>Link</a>";
                var body = "Hi,"+
                "<br/>Someone just requested to change your password, if you din't make this request, ignore this email. " +
                "<br/>" +
                "<br/>" +
                "<br/>To change your password click : <a href='http://localhost:44319/User/Resetpass/"+id+"'>Link</a>" +
                "<br/>" +
                "<br/>--" +
                "<br/>This email is automatically sent from Phiwiki web";

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)

                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true
                })
                {
                    smtp.Send(message);
                }
            }
        }



    }
}
