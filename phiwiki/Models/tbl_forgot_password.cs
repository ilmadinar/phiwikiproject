//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace phiwiki.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_forgot_password
    {
        public int id_forgot_password { get; set; }
        public string status { get; set; }
        public Nullable<int> fk_user { get; set; }
        public string name_user { get; set; }
    }
}
