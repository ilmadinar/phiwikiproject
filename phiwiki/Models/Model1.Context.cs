﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace phiwiki.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class phiwikiEntities1 : DbContext
    {
        public phiwikiEntities1()
            : base("name=phiwikiEntities1")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<tbl_forgot_password> tbl_forgot_password { get; set; }
        public DbSet<tbl_jawaban_quiz> tbl_jawaban_quiz { get; set; }
        public DbSet<tbl_layanan> tbl_layanan { get; set; }
        public DbSet<tbl_quiz> tbl_quiz { get; set; }
        public DbSet<tbl_reimburse> tbl_reimburse { get; set; }
        public DbSet<tbl_role> tbl_role { get; set; }
        public DbSet<tbl_transaksi_buku> tbl_transaksi_buku { get; set; }
        public DbSet<tbl_user> tbl_user { get; set; }
    }
}
