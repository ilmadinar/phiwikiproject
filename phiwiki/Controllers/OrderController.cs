﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using phiwiki.Models;

namespace phiwiki.Controllers
{
    public class OrderController : Controller
    {
        phiwikiEntities1 db = new phiwikiEntities1();

        public ActionResult Order()
        {
            tbl_transaksi_buku order = new tbl_transaksi_buku();
            return View(order);
        }

        [HttpPost]
        public ActionResult Order(tbl_transaksi_buku r)
        {

            if (ModelState.IsValid)
            {


                db.tbl_transaksi_buku.Add(r);
                db.SaveChanges();
                TempData["order"] = "Order successful";
                TempData.Keep();
                return RedirectToAction("Order");
            }


            return View(r);


        }


        // GET: Order
        public ActionResult Index()
        {
            return View();
        }

        // GET: Order/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Order/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Order/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Order/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Order/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Order/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Order/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
