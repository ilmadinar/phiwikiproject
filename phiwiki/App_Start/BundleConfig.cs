﻿using System.Web;
using System.Web.Optimization;

namespace phiwiki
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css1").Include(
                      "~/Temp/css/font-face.css",
                      "~/Temp/vendor/font-awesome-4.7/css/font-awesome.min.css",
                      "~/Temp/vendor/font-awesome-5/css/fontawesome-all.min.css",
                      "~/Temp/vendor/mdi-font/css/material-design-iconic-font.min.css",
                      "~/Temp/vendor/bootstrap-4.1/bootstrap.min.css",
                      "~/Temp/vendor/animsition/animsition.min.css",
                      "~/Temp/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css",
                      "~/Temp/vendor/wow/animate.css",
                      "~/Temp/vendor/css-hamburgers/hamburgers.min.css",
                      "~/Temp/vendor/slick/slick.css",
                      "~/Temp/vendor/select2/select2.min.css",
                      "~/Temp/vendor/perfect-scrollbar/perfect-scrollbar.css",
                      "~/Temp/css/theme.css"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                      "~/Temp/vendor/jquery-3.2.1.min.js",
                      "~/Temp/vendor/bootstrap-4.1/popper.min.js",
                      "~/Temp/vendor/bootstrap-4.1/bootstrap.min.js",
                      "~/Temp/vendor/slick/slick.min.js",
                      "~/Temp/vendor/wow/wow.min.js",
                      "~/Temp/vendor/animsition/animsition.min.js",
                      "~/Temp/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js",
                      "~/Temp/vendor/counter-up/jquery.waypoints.min.js",
                      "~/Temp/vendor/counter-up/jquery.counterup.min.js",
                      "~/Temp/vendor/circle-progress/circle-progress.min.js",
                      "~/Temp/vendor/perfect-scrollbar/perfect-scrollbar.js",
                      "~/Temp/vendor/chartjs/Chart.bundle.min.js",
                      "~/Temp/vendor/select2/select2.min.js",
                      "~/Temp/js/main.js"
                      ));
        }
    }
}
